#include "command_parser.h"

#include "uart_driver.h"
#include "main.h"
#include "parser_function.h"
#include <stdio.h>
#include <string.h>

extern uint8_t door_status;
extern uint8_t fan_status;
extern uint8_t heater_status;


void parse_command(uint8_t *rx_packet)
{
	// - - - Temperature Read - - - //
	if (memcmp(rx_packet, read_temperature_on_cmd, sizeof(read_temperature_on_cmd)-1) == 0) {
		update_fw_version();
		send_temperature();

	}
	// - - - Fan Read - - - //
	else if (memcmp(rx_packet, read_fan_on_cmd, sizeof(read_fan_on_cmd)-1) == 0) {
		update_fw_version();
		send_fan_status();

	// - - - Set Fan - - - //
	} else if (memcmp(rx_packet, set_fan0, sizeof(set_fan0)-1) == 0) {
		update_fw_version();
		fan_status = 0;
		send_ack();

	} else if (memcmp(rx_packet, set_fan25, sizeof(set_fan25)-1) == 0) {
		update_fw_version();
		fan_status = 25;
		send_ack();

	} else if (memcmp(rx_packet, set_fan50, sizeof(set_fan50)-1) == 0) {
		update_fw_version();
		fan_status = 50;
		send_ack();

	} else if (memcmp(rx_packet, set_fan75, sizeof(set_fan75)-1) == 0) {
		update_fw_version();
		fan_status = 75;
		send_ack();

	} else if (memcmp(rx_packet, set_fan100, sizeof(set_fan100)-1) == 0) {
		update_fw_version();
		fan_status = 100;
		send_ack();


	// - - - Set Door - - - //
	}else if (memcmp(rx_packet, open_door_on_cmd, sizeof(open_door_on_cmd)-1) == 0) {
		update_fw_version();
		door_status = 0;
		send_ack();

	} else if (memcmp(rx_packet, close_door_on_cmd, sizeof(close_door_on_cmd)-1) == 0) {
		update_fw_version();
		door_status = 1;
		send_ack();

	// - - - Read Door - - - //
	} else if (memcmp(rx_packet, read_door_on_cmd, sizeof(read_door_on_cmd)-1) == 0) {
		update_fw_version();
		send_door_status();
	}

	// - - - Read Heater - - - //
	else if (memcmp(rx_packet, read_heater_on_cmd, sizeof(read_heater_on_cmd)-1) == 0) {
		update_fw_version();
		send_heater_status();

	}


	// - - - Read RTC - - - //
	else if (memcmp(rx_packet, read_clock_on_cmd, sizeof(read_clock_on_cmd)-1) == 0) {
		update_fw_version();
		send_time();


	}

	// - - - Read FW Version - - - //
	else if (memcmp(rx_packet, read_fw_version_on_cmd, sizeof(read_fw_version_on_cmd)-1) == 0) {
		update_fw_version();
		send_fw_version();

	}

	// - - - Update FW Version - - - //
	else if (memcmp(rx_packet, fw_request_on_cmd, sizeof(fw_request_on_cmd)-1) == 0) {
		update_fw_version();
		send_fw_version();
	}
	// - - - Update and Send Every Variable - - - //
	else if (memcmp(rx_packet, report_request_on_cmd, sizeof(report_request_on_cmd)-1) == 0) {
		update_fw_version();


		send_fan_status(); //send Fan status by UART
		HAL_Delay(1);
		send_heater_status(); //send Heater status by UART
		HAL_Delay(1);
		send_door_status(); //send DOOR status by UART
		HAL_Delay(1);
		send_fan_active();
		HAL_Delay(1);
		send_alarm();
		HAL_Delay(1);
		send_temperature();

	}

	// - - - Read anything else - - - //
	else {

		send_nack();


	}
}
