#include "parser_function.h"

#include "uart_driver.h"
#include "main.h"
#include "command_parser.h"
#include <stdio.h>
#include <string.h>


extern uart_driver_t uart_driver;
extern float temperature_in_degrees;
extern uint8_t fan_status;
extern uint8_t door_status;
extern uint8_t fan_status;
extern uint8_t heater_status;
extern RTC_TimeTypeDef gTime;
extern uint8_t alarm_status;

char time_char[10];
char version_char[15];
char temperature_char[8];


int version_x = 1;
int version_y = 0;
int version_z = 0;


void update_fw_version (void){

	version_z = version_z+1;
	if (version_z == 100000000){
		version_z = 0;
		version_y = version_y + 1;
	}
	if (version_y == 10){
		version_y = 0;
		version_x = version_x + 1;
	}
	if (version_x == 10){
		version_x = 1;
	}
}

void send_fw_version (void){

	if (version_z >= 10000000){
		sprintf(version_char, "*W%d.%d.%d#", version_x, version_y, version_z);

	} else if (version_z >= 1000000){
		sprintf(version_char, "*W%d.%d.0%d#", version_x, version_y, version_z);
	} else if (version_z >= 100000){
		sprintf(version_char, "*W%d.%d.00%d#", version_x, version_y, version_z);
	} else if (version_z >= 10000){
		sprintf(version_char, "*W%d.%d.000%d#", version_x, version_y, version_z);
	} else if (version_z >= 1000){
		sprintf(version_char, "*W%d.%d.0000%d#", version_x, version_y, version_z);
	} else if (version_z >= 100){
		sprintf(version_char, "*W%d.%d.00000%d#", version_x, version_y, version_z);
	} else if (version_z >= 10){
		sprintf(version_char, "*W%d.%d.000000%d#", version_x, version_y, version_z);
	} else {
		sprintf(version_char, "*W%d.%d.0000000%d#", version_x, version_y, version_z);
	}
	uart_driver_send(&uart_driver, (uint8_t *)version_char, sizeof(version_char));
}

void send_temperature (void){
	fun_bmp280_read_temperature();
	int temperature_int = temperature_in_degrees;
	temperature_in_degrees = temperature_in_degrees * 100;
	int temperature_flt = temperature_in_degrees;
	temperature_flt = temperature_flt % 100;
	if (temperature_flt > 10){
		sprintf(temperature_char, "*T%d.%d#", temperature_int,temperature_flt);

	} else {
		sprintf(temperature_char, "*T%d.0%d#", temperature_int,temperature_flt);

	}
	uart_driver_send(&uart_driver, (uint8_t *)temperature_char, sizeof(temperature_char));

}


void send_fan_status (void){
	switch (fan_status){
		case 0:
			uart_driver_send(&uart_driver, (uint8_t *)fan0_message, sizeof(fan0_message)-1);
			break;

		case 25:
			uart_driver_send(&uart_driver, (uint8_t *)fan25_message, sizeof(fan25_message)-1);
			break;

		case 50:
			uart_driver_send(&uart_driver, (uint8_t *)fan50_message, sizeof(fan50_message)-1);
			break;

		case 75:
			uart_driver_send(&uart_driver, (uint8_t *)fan75_message, sizeof(fan75_message)-1);
			break;

		case 100:
			uart_driver_send(&uart_driver, (uint8_t *)fan100_message, sizeof(fan100_message)-1);
			break;

		default:
			break;
	}
}

void send_fan_active(void){

	if (fan_status > 0){ //send FAN ON/oFF
		uart_driver_send(&uart_driver, (uint8_t *)fanon_message, sizeof(fanon_message)-1);
	} else {
		uart_driver_send(&uart_driver, (uint8_t *)fanoff_message, sizeof(fanoff_message)-1);
	}
}

void send_door_status (void){
	if (door_status == 1){
		uart_driver_send(&uart_driver, (uint8_t *)door_close_message, sizeof(door_close_message)-1);
	} else {
		uart_driver_send(&uart_driver, (uint8_t *)door_open_message, sizeof(door_open_message)-1);
	}
}

void send_heater_status (void){
	if (heater_status == 1){
		uart_driver_send(&uart_driver, (uint8_t *)heater_on_message, sizeof(heater_on_message)-1);
	} else {
		uart_driver_send(&uart_driver, (uint8_t *)heater_off_message, sizeof(heater_off_message)-1);
	}
}

void send_time (void){

	read_time();
	if((gTime.Hours<10)&&(gTime.Minutes<10)&&(gTime.Seconds<10)){
		sprintf(time_char, "*0%d:0%d:0%d#", gTime.Hours, gTime.Minutes, gTime.Seconds);
	} else if((gTime.Hours<10)&&(gTime.Minutes<10)&&(gTime.Seconds>10)){
		sprintf(time_char, "*0%d:0%d:%d#", gTime.Hours, gTime.Minutes, gTime.Seconds);
	} else if((gTime.Hours<10)&&(gTime.Minutes>10)&&(gTime.Seconds<10)){
		sprintf(time_char, "*0%d:%d:0%d#", gTime.Hours, gTime.Minutes, gTime.Seconds);
	} else if((gTime.Hours>10)&&(gTime.Minutes<10)&&(gTime.Seconds<10)){
		sprintf(time_char, "*%d:0%d:0%d#", gTime.Hours, gTime.Minutes, gTime.Seconds);
	} else if((gTime.Hours<10)&&(gTime.Minutes>10)&&(gTime.Seconds>10)){
		sprintf(time_char, "*0%d:%d:%d#", gTime.Hours, gTime.Minutes, gTime.Seconds);
	} else if((gTime.Hours>10)&&(gTime.Minutes<10)&&(gTime.Seconds>10)){
		sprintf(time_char, "*%d:0%d:%d#", gTime.Hours, gTime.Minutes, gTime.Seconds);
	} else if((gTime.Hours>10)&&(gTime.Minutes>10)&&(gTime.Seconds<10)){
		sprintf(time_char, "*%d:%d:0%d#", gTime.Hours, gTime.Minutes, gTime.Seconds);
	} else{
		sprintf(time_char, "*%d:%d:%d#", gTime.Hours, gTime.Minutes, gTime.Seconds);
	}
	uart_driver_send(&uart_driver, (uint8_t *)time_char, sizeof(time_char));
}

void send_alarm (void){

	if (alarm_status == 1){ //alarm FAN ON/oFF
		uart_driver_send(&uart_driver, (uint8_t *)alarmon_message, sizeof(alarmon_message)-1);
	} else {
		uart_driver_send(&uart_driver, (uint8_t *)alarmoff_message, sizeof(alarmoff_message)-1);
	}
}

void send_ack (void){
	uart_driver_send(&uart_driver, (uint8_t *)ack_message, sizeof(ack_message)-1);
}

void send_nack (void){
	uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message)-1);
}
