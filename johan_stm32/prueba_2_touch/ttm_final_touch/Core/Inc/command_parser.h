#ifndef __COMMAND_PARSER_INC_
#define __COMMAND_PARSER_INC_

#include <stdint.h>
#include <stdlib.h>



#define PROTOCOL_PREAMBLE '*'
#define PROTOCOL_POSAMBLE '#'


// - - - Messajes - - - //

#define ack_message "*ACK#"
#define  nack_message "*NACK#"

#define  fan0_message "*F000#"
#define  fan25_message "*F025#"
#define  fan50_message "*F050#"
#define  fan75_message "*F075#"
#define  fan100_message "*F100#"

#define  fanoff_message "*f0#"
#define  fanon_message "*f1#"

#define  alarmoff_message "*a0#"
#define  alarmon_message "*a1#"

#define  door_open_message "*D0#"
#define  door_close_message "*D1#"

#define  heater_on_message "*H1#"
#define  heater_off_message "*H0#"

#define  preamble_sign "*"
#define  posamble_sign "#"


// - - - Commands - - - //


#define  read_temperature_on_cmd "READ_TEMPERATURE"

#define  read_fan_on_cmd "READ_FAN"
#define  set_fan0 "SET_FAN0"
#define  set_fan25 "SET_FAN25"
#define  set_fan50 "SET_FAN50"
#define  set_fan75 "SET_FAN75"
#define  set_fan100 "SET_FAN100"

#define  open_door_on_cmd "OPEN_DOOR"
#define  close_door_on_cmd "CLOSE_DOOR"
#define  read_door_on_cmd "READ_DOOR"

#define  read_heater_on_cmd "READ_HEATER"

#define  read_fw_version_on_cmd "READ_VERSION"

#define  read_clock_on_cmd "READ_CLOCK"

#define  fw_request_on_cmd "FW_REQUEST"

#define  report_request_on_cmd "REPORT_REQUEST"



void parse_command(uint8_t *);

#endif /* __COMMAND_PARSER_INC_ */
