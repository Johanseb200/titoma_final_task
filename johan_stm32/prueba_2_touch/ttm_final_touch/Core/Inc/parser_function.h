#ifndef __PARSER_FUNCTION_INC_
#define __PARSER_FUNCTION_INC_

#include <stdint.h>
#include <stdlib.h>


void send_fw_version();
void update_fw_version();

void send_temperature();
void send_fan_status();
void send_fan_active();
void send_door_status();
void send_heater_status();
void send_ack();
void send_nack();
void send_alarm();
void send_time();

#endif /* __PARSER_FUNCTION_INC_ */
