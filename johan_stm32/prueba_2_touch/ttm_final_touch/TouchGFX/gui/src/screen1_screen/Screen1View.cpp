#include <gui/screen1_screen/Screen1View.hpp>
#include "main.h"

extern uint8_t door_status;

Screen1View::Screen1View()
{

}

void Screen1View::setupScreen()
{
    Screen1ViewBase::setupScreen();
}

void Screen1View::tearDownScreen()
{
    Screen1ViewBase::tearDownScreen();
}

void Screen1View::open_the_door(){
	if (door == 0){
		door = !door;
		door_status = 1;
		Unicode::snprintf(door_status_lcdBuffer, DOOR_STATUS_LCD_SIZE, "open   ");
		door_status_lcd.resizeToCurrentText();
		door_status_lcd.invalidate();

	} else{
		door = !door;
		door_status = 0;
		Unicode::snprintf(door_status_lcdBuffer, DOOR_STATUS_LCD_SIZE, "cloSed");
		door_status_lcd.resizeToCurrentText();
		door_status_lcd.invalidate();

	}



}

void Screen1View::updateTemp(uint16_t new_temp){
	temp_status_lcd.resizeToCurrentText();
	temp_status_lcd.invalidate();
	uint16_t new_temp_f;
	new_temp_f = new_temp % 100;
	new_temp = new_temp / 100;
	Unicode::snprintf(temp_status_lcdBuffer, TEMP_STATUS_LCD_SIZE, "%d.%d", new_temp, new_temp_f);
	temp_status_lcd.resizeToCurrentText();
	temp_status_lcd.invalidate();
}

void Screen1View::updateFan(uint8_t fandisplay){
	fan_status_lcd.resizeToCurrentText();
	fan_status_lcd.invalidate();
	Unicode::snprintf(fan_status_lcdBuffer, FAN_STATUS_LCD_SIZE, "%d",fandisplay);
	fan_status_lcd.resizeToCurrentText();
	fan_status_lcd.invalidate();
}

void Screen1View::updateHeater(uint8_t heaterdisplay){
	heater_status_lcd.resizeToCurrentText();
	heater_status_lcd.invalidate();
	Unicode::snprintf(heater_status_lcdBuffer, HEATER_STATUS_LCD_SIZE, "%d",heaterdisplay);
	heater_status_lcd.resizeToCurrentText();
	heater_status_lcd.invalidate();
}

void Screen1View::updateVersion(uint32_t versiondisplay){
	version_status_lcd.resizeToCurrentText();
	version_status_lcd.invalidate();

	if (versiondisplay >= 10000000){
		Unicode::snprintf(version_status_lcdBuffer, VERSION_STATUS_LCD_SIZE, "1.1.%d", versiondisplay);

	} else if (versiondisplay >= 1000000){
		Unicode::snprintf(version_status_lcdBuffer, VERSION_STATUS_LCD_SIZE, "1.1.0%d", versiondisplay);
	} else if (versiondisplay >= 100000){
		Unicode::snprintf(version_status_lcdBuffer, VERSION_STATUS_LCD_SIZE, "1.1.00%d", versiondisplay);
	} else if (versiondisplay >= 10000){
		Unicode::snprintf(version_status_lcdBuffer, VERSION_STATUS_LCD_SIZE, "1.1.000%d", versiondisplay);
	} else if (versiondisplay >= 1000){
		Unicode::snprintf(version_status_lcdBuffer, VERSION_STATUS_LCD_SIZE, "1.1.0000%d", versiondisplay);
	} else if (versiondisplay >= 100){
		Unicode::snprintf(version_status_lcdBuffer, VERSION_STATUS_LCD_SIZE, "1.1.00000%d", versiondisplay);
	} else if (versiondisplay >= 10){
		Unicode::snprintf(version_status_lcdBuffer, VERSION_STATUS_LCD_SIZE, "1.1.000000%d", versiondisplay);
	} else {
		Unicode::snprintf(version_status_lcdBuffer, VERSION_STATUS_LCD_SIZE, "1.1.0000000%d", versiondisplay);
	}
	//Unicode::snprintf(version_status_lcdBuffer, VERSION_STATUS_LCD_SIZE, "%d", versiondisplay);
	version_status_lcd.resizeToCurrentText();
	version_status_lcd.invalidate();
}
