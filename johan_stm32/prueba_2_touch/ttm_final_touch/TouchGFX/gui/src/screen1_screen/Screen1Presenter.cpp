#include <gui/screen1_screen/Screen1View.hpp>
#include <gui/screen1_screen/Screen1Presenter.hpp>

Screen1Presenter::Screen1Presenter(Screen1View& v)
    : view(v)
{

}

void Screen1Presenter::activate()
{

}

void Screen1Presenter::deactivate()
{

}

void Screen1Presenter::setNewTemp(uint16_t tempdisplay){
	view.updateTemp(tempdisplay);

}

void Screen1Presenter::setNewFan(uint8_t fandisplay){
	view.updateFan(fandisplay);

}
void Screen1Presenter::setNewHeater(uint8_t heaterdisplay){
	view.updateHeater(heaterdisplay);

}
void Screen1Presenter::setNewVersion(uint32_t versiondisplay){
	view.updateVersion(versiondisplay);

}

