#ifndef SCREEN1VIEW_HPP
#define SCREEN1VIEW_HPP

#include <gui_generated/screen1_screen/Screen1ViewBase.hpp>
#include <gui/screen1_screen/Screen1Presenter.hpp>
#include "stdint.h"
#include "string.h"

class Screen1View : public Screen1ViewBase
{
public:
	uint8_t door = 0;
	char mensaje_de_puerta[6];
    Screen1View();
    virtual ~Screen1View() {}
    virtual void setupScreen();
    virtual void tearDownScreen();

    virtual void open_the_door();

    void updateTemp(uint16_t);

    void updateFan(uint8_t);
    void updateHeater(uint8_t);
    void updateVersion(uint32_t);
protected:
};

#endif // SCREEN1VIEW_HPP
