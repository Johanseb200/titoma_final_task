#ifndef MODELLISTENER_HPP
#define MODELLISTENER_HPP

#include <gui/model/Model.hpp>
#include "stdint.h"
#include "string.h"

class ModelListener
{
public:
    ModelListener() : model(0) {}
    
    virtual ~ModelListener() {}

    void bind(Model* m)
    {
        model = m;
    }

    virtual void setNewTemp(uint16_t tempdisplay) {};
    virtual void setNewFan(uint8_t fandisplay) {};
    virtual void setNewHeater(uint8_t heaterdisplay) {};
    virtual void setNewVersion(uint32_t versiondisplay) {};
protected:
    Model* model;
};

#endif // MODELLISTENER_HPP
