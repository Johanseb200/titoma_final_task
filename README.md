# Wireless Temperature Room Controller
<br/>

Titoma Firmware Cathedra Final Assignment

Johan Sebastian Gomez Lugo jsgomezlu@unal.edu.co

<br/>

## Introduction




This project consist in a variable control system based on temperaute, this project has SPI communication to configure and
obtain the data of the temperature in a room through a sensor (bmp280), in addition to a series of controlled variables GPIOS of the microcontroller
to represent a door an alarm and a pulse to see that the system works correctly, it has a system to regulate the temperature
through a HEATER and a FAN controlled by a PWM.

<br/>

## Before start

check if the project pinout is as the follow

|  STM32 	|   BMP280	|
|---	|---	|
|   3.3V	|  VCC 	|
|   GND	|   GND	|
|   PB3	|   SCL	|
|  PA7  |   SDA	|
|   CSB	|   PD6	|
|   SDO	|   PB4	|


|  STM32 	|   ESP32	|
|---	|---	|
|   PA9	|  RX2 	|
|   PA10	|   TX2	|


<br/>

## Aplication

For the control and operation of this system the user can both control and communicate through the UART protocol, this serial communication
It is subject to a series of commands for reading and writing system data, these commands are

- "READ_TEMPERATURE"
- "READ_FAN"
- "SET_FAN0"
- "SET_FAN25"
- "SET_FAN50"
- "SET_FAN75"
- "SET_FAN100"
- "OPEN_DOOR"
- "CLOSE_DOOR"
- "READ_DOOR"
- "READ_HEATER"
- "READ_VERSION"
- "READ_CLOCK"

This system will communicate to a mqtt platform, through the connection with an esp32 module when the user press the Update button the system will send a
"REPORT_REQUEST", and then the server will read and update variables (Poner las variables que van a ser updateadas)

The project also contains a LCD display that show in real time the state of the variable sistem and have a button that can be pressed in order to change the door state